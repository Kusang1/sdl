#include "ResourceManager.hpp"
#include "ImageLoader.hpp"
#include "MLogger.hpp"

std::map<std::string, GLTexture> ResourceManager::_textureMap;

void ResourceManager::loadTexture(const std::string& name, const std::string& path)
{
    if (!isTextureExists(name))
    {
        MLogger::log("Load image " + name + " from " + path);
        GLTexture texture = ImageLoader::loadPNG(path);
        _textureMap[name] = texture;
    }
    else
    {
        MLogger::error("Texture already exists: " + name);
    }
}

GLTexture ResourceManager::getTexture(const std::string& name)
{
    if (!isTextureExists(name))
    {
        MLogger::error("cant' find texture " + name);
    }
    // MLogger::log("Get image " + name);
    return _textureMap[name];
}

bool ResourceManager::isTextureExists(const std::string& name)
{
    auto find = _textureMap.find(name);
    return find != _textureMap.end();
}