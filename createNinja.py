import glob
import os
from ninja_syntax import Writer

cxx_files = glob.glob("*.cpp")
obj_files = []
target = "foobar"

with open("build.ninja", "w") as buildfile:
    n = Writer(buildfile)

    n.variable("cxx", "clang++")
    n.variable("cflags", "-std=c++11 -g -O0 ")
    n.variable("libs", "-lGL -lGLEW -lSDL2")
    n.newline()

    n.rule("compile",
        command="$cxx $cflags -c $in -o $out",
        description="COMPILE $out"
    )
    n.newline()

    n.rule("link",
        command="$cxx $ldflags -o $out $in $libs",
        description="LINK $out")
    n.newline()

    for src in cxx_files:
        obj = "build/" + os.path.splitext(src)[0] + '.o'
        obj_files.append(obj)
        n.build(obj, 'compile', src)
    n.newline()

    n.build(target, 'link', obj_files)
    n.build('all', 'phony', [target])

    n.default('all')


