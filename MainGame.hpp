#pragma once
#include <vector>
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include "Sprite.hpp"
#include "GLProgram.hpp"
#include "GLTexture.hpp"
#include "Camera2d.hpp"
#include "SpriteBatch.hpp"
#include "InputManager.hpp"

enum class GameState { PLAY, EXIT };

class MainGame
{
public:
    MainGame();
    ~MainGame();

    void run();

private:
    void initSystems();
    void processInput();
    void gameLoop();
    void draw();
    void calculateFPS();

private:
    const int WIDTH;
    const int HEIGHT;
    const float MAX_FPS = 60.0f;

    SDL_Window* _window;
    GameState _gameState;
    float _time;
    GLTexture _texture;
    std::vector<Sprite*> _sprites;
    GLProgram _shader;
    Camera2d _camera;
    SpriteBatch _batch;
    float _fps;
    float _frameTime;
    uint32_t _currentTick;
    InputManager _inputManager;
};