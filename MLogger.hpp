#include <iostream>
#include <string>

class MLogger
{
public:
    static inline void error(const std::string& message)
    {
        std::cout << message << std::endl;
        exit(-1);
    }

    static inline void log(const std::string& message)
    {
        std::cout << message << std::endl;
    }
};