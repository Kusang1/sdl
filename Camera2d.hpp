#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Camera2d
{
public:
    explicit Camera2d(int width, int height);
    Camera2d(Camera2d&) = delete;
    Camera2d(Camera2d&&) = delete;
    ~Camera2d();

    void update();

    void setScale(float scale);
    float getScale();

    void setPosition(const glm::vec2& position);
    glm::vec2 getPosition();

    glm::mat4 getMatrix();
    glm::vec2 getWorldPositionFromScreen(glm::vec2 screenPosition);

private:
    int _width;
    int _height;
    float _scale;
    bool _needsUpdate;
    glm::vec2 _position;
    glm::mat4 _cameraMatrix;
    glm::mat4 _orthoMatrix;
};