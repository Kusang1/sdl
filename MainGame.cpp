#include "MainGame.hpp"
#include "MLogger.hpp"
#include "ImageLoader.hpp"
#include "ResourceManager.hpp"
#include <glm/glm.hpp>

MainGame::MainGame() :
    WIDTH(800),
    HEIGHT(600),
    _window(nullptr),
    _gameState(GameState::PLAY),
    _time(0.0f),
    _camera(WIDTH, HEIGHT),
    _fps(0.0f),
    _frameTime(0.0f)
{

}

MainGame::~MainGame()
{
    // if (!_sprites.empty())
    // {
    //     for (auto& sprite : _sprites)
    //     {
    //         delete sprite;
    //     }
    // }
}

void MainGame::run()
{
    initSystems();
    ResourceManager::loadTexture("tile", "assets/tile.png");
    // _sprites.push_back(new Sprite());
    // _sprites.push_back(new Sprite());
    // _sprites.push_back(new Sprite());
    // _sprites.push_back(new Sprite());

    // _sprites[0]->init(0, 0, 100, 100, "tile");
    // _sprites[1]->init(0.0f, 100, 100, 100, "tile");
    // _sprites[2]->init(0.0f, 100, 100, 100, "tile");
    // _sprites[2]->init(-1.0f, 0.0f, 1.0f, 1.0f, "tile");
    // _texture = ImageLoader::loadPNG("assets/tile.png");
    gameLoop();
}

void MainGame::initSystems()
{
    SDL_Init(SDL_INIT_EVERYTHING);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    _window = SDL_CreateWindow("Game Engine", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL);
    if (_window == nullptr)
    {
        MLogger::error("Failed to create a window");
    }
    SDL_GLContext glContext = SDL_GL_CreateContext(_window);
    if (glContext == nullptr)
    {
        MLogger::error("Failed to create GL Context");
    }
    glewExperimental = GL_TRUE;
    GLenum error = glewInit();
    if (error != GLEW_OK)
    {
        MLogger::error("Coudn't initalize GLEW");
    }
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    SDL_GL_SetSwapInterval(1);

    glClearColor(0.3f, 0.3f, 1.0f, 1.0f);
    _shader.compileShaders("shaders/vertex.vert", "shaders/fragment.frag");
    _shader.addAttribute("position");
    _shader.addAttribute("vertexColor");
    _shader.addAttribute("uv");
    _shader.linkProgram();

    _batch.init();
}

void MainGame::processInput()
{
    SDL_Event ev;
    while (SDL_PollEvent(&ev))
    {
        switch (ev.type)
        {
            case SDL_QUIT:
                _gameState = GameState::EXIT;
                break;

            case SDL_KEYDOWN:
                _inputManager.pressKey(ev.key.keysym.sym);
                break;

            case SDL_KEYUP:
                _inputManager.releaseKey(ev.key.keysym.sym);
                break;

            case SDL_MOUSEBUTTONDOWN:
                _inputManager.pressKey(ev.button.button);
                break;

            case SDL_MOUSEBUTTONUP:
                _inputManager.releaseKey(ev.button.button);
                break;

            case SDL_MOUSEMOTION:
                _inputManager.setMousePosition(ev.motion.x, ev.motion.y);
                break;
        }
    }

    if (_inputManager.isKeyPressed(SDLK_q) || _inputManager.isKeyPressed(SDLK_ESCAPE))
    {
        _gameState = GameState::EXIT;
    }

    if (_inputManager.isKeyPressed(SDLK_w))
    {
        _camera.setPosition(_camera.getPosition() + glm::vec2(0.0f, -10.0f));
    }

    if (_inputManager.isKeyPressed(SDLK_s))
    {
        _camera.setPosition(_camera.getPosition() + glm::vec2(0.0f, 10.0f));
    }

    if (_inputManager.isKeyPressed(SDLK_a))
    {
        _camera.setPosition(_camera.getPosition() + glm::vec2(10.0f, 0.0f));
    }

    if (_inputManager.isKeyPressed(SDLK_d))
    {
        _camera.setPosition(_camera.getPosition() + glm::vec2(-10.0f, 0.0f));
    }

    if (_inputManager.isKeyPressed(SDL_BUTTON_LEFT))
    {
        glm::vec2 position = _inputManager.getMousePosition();
        std::cout << position.x << " " << position.y << std::endl;
    }
}

void MainGame::gameLoop()
{
    while (_gameState == GameState::PLAY)
    {
        calculateFPS();

        _time += 0.016;
        processInput();

        _camera.update();
        draw();
    }
}

void MainGame::draw()
{
    glClearDepth(1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    _shader.bind();
    glActiveTexture(GL_TEXTURE0);

    GLint projectionLocation = _shader.getUniformLocation("projection");
    glm::mat4 cameraMatrix = _camera.getMatrix();
    // std::cout << cameraMatrix << std::endl;
    glUniformMatrix4fv(projectionLocation, 1, GL_FALSE, &cameraMatrix[0][0]);

    // for (const auto& sprite : _sprites)
    // {
    //     sprite->draw();
    // }
    glBindTexture(GL_TEXTURE_2D, 0);
    // glBindTexture(GL_TEXTURE_2D, _texture.id);
    GLint textureLocation = _shader.getUniformLocation("myTexture");
    glUniform1i(textureLocation, 0);
    _batch.begin();


    _batch.draw(
        glm::vec4(50.0f, 50.0f, 50.0f, 50.0f),
        glm::vec4(0.0, 0.0, 1.0f, 1.0f),
        1.0f,
        ResourceManager::getTexture("tile").id,
        {255, 255, 255}
    );
    _batch.end();
    _batch.renderBatch();
    _shader.unbind();

    SDL_GL_SwapWindow(_window);
}

void MainGame::calculateFPS()
{
    static float previousTicks = SDL_GetTicks();
    float currentTicks = SDL_GetTicks();

    _frameTime = currentTicks - previousTicks;
    // std::cout << 1000.0 / _frameTime << std::endl;
    previousTicks = currentTicks;
}