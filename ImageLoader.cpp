#include <vector>
#include "ImageLoader.hpp"
#include "picopng.hpp"
#include "IOManager.hpp"
#include "MLogger.hpp"

GLTexture ImageLoader::loadPNG(const std::string& path)
{
    GLTexture texture = {};

    std::vector<unsigned char> out;
    unsigned long width;
    unsigned long height;

    std::vector<unsigned char> png;
    if (!IOManager::readFileToBuffer(path, png))
    {
        MLogger::error("can't read file: " + path);
    }

    int error = decodePNG(out, width, height, png.data(), png.size());
    if (error != 0)
    {
        MLogger::error("cant't decode png: " + path);
    }

    glGenTextures(1, &texture.id);
    glBindTexture(GL_TEXTURE_2D, texture.id);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &(out[0]));

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);

    texture.width = width;
    texture.height = height;

    return texture;
}