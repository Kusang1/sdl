#include "GLProgram.hpp"
#include "MLogger.hpp"
#include <fstream>
#include <vector>
#include <iostream>

GLProgram::GLProgram() : _programID(0),
    _vertexID(0),
    _fragmentID(0),
    _numAttributes(0)
{

}

GLProgram::~GLProgram()
{

}

void GLProgram::compileShaders(const std::string& vertexPath, const std::string& fragmentPath)
{
    _programID = glCreateProgram();
    if (_programID == 0)
    {
        MLogger::error("failed to create shader program");
    }
    _vertexID = glCreateShader(GL_VERTEX_SHADER);
    if (_vertexID == 0)
    {
        MLogger::error("failed to create vertex shader");
    }
    _fragmentID = glCreateShader(GL_FRAGMENT_SHADER);
    if (_fragmentID == 0)
    {
        MLogger::error("failed to create fragment shader");
    }

    compileShader(vertexPath, _vertexID);
    compileShader(fragmentPath, _fragmentID);
}
void GLProgram::linkProgram()
{
    glAttachShader(_programID, _vertexID);
    glAttachShader(_programID, _fragmentID);

    glLinkProgram(_programID);

    GLint isLinked = 0;
    glGetProgramiv(_programID, GL_LINK_STATUS, &isLinked);
    if (isLinked == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetProgramiv(_programID, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        std::vector<GLchar> infoLog(maxLength);
        glGetProgramInfoLog(_programID, maxLength, &maxLength, &infoLog[0]);

        // The program is useless now. So delete it.
        glDeleteProgram(_programID);
        std::printf("%s\n", &(infoLog[0]));
        MLogger::error("failed to link shader program");
    }
    glDetachShader(_programID, _vertexID);
    glDetachShader(_programID, _fragmentID);
    glDeleteShader(_vertexID);
    glDeleteShader(_fragmentID);
}

std::string GLProgram::readFile(const std::string& path) const
{
    std::ifstream fp(path);
    if (fp.fail())
    {
        perror(path.c_str());
        MLogger::error("Failed to open file: " + path);
    }
    std::string content;
    std::string line;

    while (std::getline(fp, line))
    {
        content += line;
        content += "\n";
    }

    return content;
}

void GLProgram::checkCompileErrors(GLuint shader)
{
    GLint success = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (success == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

        std::vector<char> errorLog(maxLength);
        glGetShaderInfoLog(shader, maxLength, &maxLength, &errorLog[0]);
        glDeleteShader(shader);
        std::printf("%s\n", errorLog.data());
        MLogger::error("Shader failed to compile");
    }
}

void GLProgram::compileShader(const std::string& path, GLuint id)
{
    std::string vertexSource = readFile(path);
    const char* vertexSourcePointer = vertexSource.c_str();
    glShaderSource(id, 1, &vertexSourcePointer, nullptr);
    glCompileShader(id);
    checkCompileErrors(id);
}

void GLProgram::addAttribute(const std::string& attribute)
{
    glBindAttribLocation(_programID, _numAttributes, attribute.c_str());
    _numAttributes++;
}

void GLProgram::bind() const
{
    glUseProgram(_programID);
    // for (int i = 0; i < _numAttributes; i++)
    // {
    //     glEnableVertexAttribArray(i);
    // }
}

void GLProgram::unbind() const
{
    // for (int i = 0; i < _numAttributes; i++)
    // {
    //     glDisableVertexAttribArray(i);
    // }
    glUseProgram(0);
}

GLuint GLProgram::getUniformLocation(const std::string& name)
{
    GLuint location = glGetUniformLocation(_programID, name.c_str());
    if (location == GL_INVALID_INDEX)
    {
        MLogger::error("Can't find uniform " + name);
    }
    return location;
}