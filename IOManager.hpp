#pragma once
#include <vector>
#include <string>
class IOManager
{
public:
    static bool readFileToBuffer(const std::string& path, std::vector<unsigned char>& buffer);
};