#include "InputManager.hpp"

InputManager::InputManager() :
    _mousePosition(0.0f)
{

}

InputManager::~InputManager()
{

}

void InputManager::pressKey(unsigned int keyID)
{
    _keyMap[keyID] = true;
}

void InputManager::releaseKey(unsigned int keyID)
{
    _keyMap[keyID] = false;
}

bool InputManager::isKeyPressed(unsigned int keyID)
{
    const auto& element = _keyMap.find(keyID);
    if (element == _keyMap.end())
    {
        return false;
    }
    return element->second;
}

void InputManager::setMousePosition(float x, float y)
{
    _mousePosition.x = x;
    _mousePosition.y = y;
}

glm::vec2 InputManager::getMousePosition() const
{
    return _mousePosition;
}