#pragma once
#include <string>
#include "GLTexture.hpp"

class ImageLoader
{
public:
    static GLTexture loadPNG(const std::string& path);
};