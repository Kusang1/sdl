#include <iostream>
#include "MainGame.hpp"

int main()
{
    MainGame game;
    game.run();

    return 0;
}