#pragma once
#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "Vertex.hpp"

enum class GlyphSortType
{
    NONE,
    FRONT_TO_BACK,
    BACK_TO_FRONT,
    TEXTURE
};

struct Glyph
{
    GLuint texture;
    float depth;

    Vertex topLeft;
    Vertex topRight;

    Vertex bottomLeft;
    Vertex bottomRight;
};

struct RenderBatch
{
    RenderBatch(GLuint offset_, GLuint num_, GLuint texture_) : offset(offset_), numVerticies(num_), texture(texture_) {}


    GLuint offset;
    GLuint numVerticies;
    GLuint texture;
};

class SpriteBatch
{
public:
    explicit SpriteBatch(GlyphSortType sortType = GlyphSortType::TEXTURE);
    ~SpriteBatch();

    void init();

    void begin();
    void end();

    void draw(const glm::vec4& destRect, const glm::vec4& srcRect, float depth, GLuint texture, const Color& color);
    void renderBatch();

private:
    void createVertexArrayBuffer();
    void sortGlyphs();
    void createRenderBatches();

private:
    GLuint _vbo;
    GLuint _vao;
    GlyphSortType _sortType;

    std::vector<Glyph*> _glyphs;
    std::vector<RenderBatch> _renderBatches;
};