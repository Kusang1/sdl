#include "Sprite.hpp"
#include "Vertex.hpp"
#include "ResourceManager.hpp"

Sprite::Sprite() : _x(0),
    _y(0),
    _width(0),
    _height(0),
    _vboID(0)
{

}

Sprite::~Sprite()
{
    if (_vboID != 0)
    {
        glDeleteBuffers(1, &_vboID);
    }
}

void Sprite::init(float x, float y, float width, float height, const std::string& textureName)
{
    _x = x;
    _y = y;
    _width = width;
    _height = height;

    if (_vboID == 0)
    {
        glGenBuffers(1, &_vboID);
    }

    Vertex vertexData[6];
    vertexData[0].position.x = x + width;
    vertexData[0].position.y = y + height;
    vertexData[0].setUV(1.0f, 0.0f);

    vertexData[1].position.x = x;
    vertexData[1].position.y = y + height;
    vertexData[1].setUV(0.0f, 0.0f);

    vertexData[2].position.x = x;
    vertexData[2].position.y = y;
    vertexData[2].setUV(0.0f, 1.0f);

    vertexData[3].position.x = x;
    vertexData[3].position.y = y;
    vertexData[3].setUV(0.0f, 1.0f);

    vertexData[4].position.x = x + width;
    vertexData[4].position.y = y;
    vertexData[4].setUV(1.0f, 1.0f);

    vertexData[5].position.x = x + width;
    vertexData[5].position.y = y + height;
    vertexData[5].setUV(1.0f, 0.0f);

    for (int i = 0; i < 6; i++)
    {
        vertexData[i].color.r = 255;
        vertexData[i].color.g = 255;
        vertexData[i].color.b = 255;
        vertexData[i].color.a = 255;
    }

    vertexData[1].color.r = 0;
    vertexData[4].color.g = 0;

    glBindBuffer(GL_ARRAY_BUFFER, _vboID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    _texture = ResourceManager::getTexture(textureName);
}

void Sprite::draw()
{
    // glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _texture.id);
    // GLint textureLocation = _shader.getUniformLocation("myTexture");
    // glUniform1i(textureLocation, 0);

    glBindBuffer(GL_ARRAY_BUFFER, _vboID);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));
    glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), (void*)offsetof(Vertex, color));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, uv));
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}