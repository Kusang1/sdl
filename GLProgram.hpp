#pragma once
#include <string>
#include <GL/glew.h>

class GLProgram
{
public:
    GLProgram();
    ~GLProgram();

    void compileShaders(const std::string& vertexPath, const std::string& fragmentPath);
    void linkProgram();
    void addAttribute(const std::string& attribute);
    void bind() const;
    void unbind() const;
    GLuint getUniformLocation(const std::string& name);
private:
    std::string readFile(const std::string& path) const;
    void checkCompileErrors(GLuint shader);
    void compileShader(const std::string& path, GLuint id);
private:
    GLuint _programID;
    GLuint _vertexID;
    GLuint _fragmentID;
    GLuint _numAttributes;
};