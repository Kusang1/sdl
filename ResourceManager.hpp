#pragma once
#include <string>
#include <map>
#include "GLTexture.hpp"

class ResourceManager
{
public:
    static void loadTexture(const std::string& name, const std::string& path);
    static GLTexture getTexture(const std::string& name);

private:
    static bool isTextureExists(const std::string& name);

public:
    static std::map<std::string, GLTexture> _textureMap;
};