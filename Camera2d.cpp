#include "Camera2d.hpp"

Camera2d::Camera2d(int width, int height) :
    _width(width),
    _height(height),
    _scale(1.0f),
    _needsUpdate(true),
    _position(0.0f),
    _cameraMatrix(1.0f)
{
    _orthoMatrix = glm::ortho(0.0f, static_cast<float>(_width), 0.0f, static_cast<float>(_height));
}

Camera2d::~Camera2d()
{

}

void Camera2d::update()
{
    if (_needsUpdate)
    {
        glm::vec3 translate(-_position.x, -_position.y, 0.0f);
        _cameraMatrix = glm::translate(_orthoMatrix, translate);
        _cameraMatrix = glm::scale(_cameraMatrix, glm::vec3(_scale, _scale, 1.0f));
        _needsUpdate = false;
    }
}

void Camera2d::setPosition(const glm::vec2& position)
{
    _position = position;
    _needsUpdate = true;
}

glm::vec2 Camera2d::getPosition()
{
    return _position;
}

void Camera2d::setScale(float scale)
{
    _scale = scale;
    _needsUpdate = true;
}

float Camera2d::getScale()
{
    return _scale;
}

glm::mat4 Camera2d::getMatrix()
{
    return _cameraMatrix;
}

glm::vec2 Camera2d::getWorldPositionFromScreen(glm::vec2 screenPosition)
{
    screenPosition -= glm::vec2(_width / 2, _height / 2);
    screenPosition /= _scale;
    screenPosition += _position;

    return screenPosition;
}