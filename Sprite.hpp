#pragma once
#include <string>
#include <GL/glew.h>
#include "GLTexture.hpp"

class Sprite
{
public:
    Sprite();
    ~Sprite();

    void init(float x, float y, float width, float height, const std::string& textureName);
    void draw();
private:
    float _x;
    float _y;
    float _width;
    float _height;

    GLuint _vboID;
    GLTexture _texture;
};