#version 130

in vec2 position;
in vec4 vertexColor;
in vec2 uv;

// uniform float time;
uniform sampler2D myTexture;
uniform mat4 projection;

out vec4 fragmentColor;
out vec2 fragmentUV;

void main()
{
    gl_Position = projection * vec4(position, 0.0, 1.0);
    fragmentColor = vertexColor;
    fragmentUV = vec2(uv.x, uv.y);
}