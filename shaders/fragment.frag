#version 130

in vec4 fragmentColor;
in vec2 fragmentUV;
out vec4 color;
// uniform float time;
uniform sampler2D myTexture;
uniform mat4 projection;
void main()
{
    // color = fragmentColor - vec4(0.5 * cos(time) + 1.0, 0.5 * sin(time) + 1.0, 0.0, 0.0);
    color = texture(myTexture, fragmentUV);
}