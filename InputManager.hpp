#pragma once
#include <unordered_map>
#include <glm/glm.hpp>

class InputManager
{
public:
    explicit InputManager();
    ~InputManager();

    void pressKey(unsigned int keyID);
    void releaseKey(unsigned int keyID);
    bool isKeyPressed(unsigned int keyID);
    void setMousePosition(float x, float y);
    glm::vec2 getMousePosition() const;

private:
    std::unordered_map<unsigned int, bool> _keyMap;
    glm::vec2 _mousePosition;
};