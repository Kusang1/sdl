#include "IOManager.hpp"
#include <fstream>

bool IOManager::readFileToBuffer(const std::string& path, std::vector<unsigned char>& buffer)
{
    std::ifstream file(path.c_str(), std::ios::in|std::ios::binary|std::ios::ate);

    if (file.fail())
    {
        return false;
    }
    //get filesize
    std::streamsize size = 0;
    if(file.seekg(0, std::ios::end).good()) size = file.tellg();
    if(file.seekg(0, std::ios::beg).good()) size -= file.tellg();

    //read contents of the file into the vector
    if(size > 0)
    {
        buffer.resize((size_t)size);
        file.read((char*)(&buffer[0]), size);
    }
    else
    {
        buffer.clear();
    }
    return true;
}