#include "SpriteBatch.hpp"
#include <algorithm>

SpriteBatch::SpriteBatch(GlyphSortType sortType) :
    _vbo(0),
    _vao(0),
    _sortType(sortType)
{

}

SpriteBatch::~SpriteBatch()
{
    // glBindVertexArray(0);
    if (!_glyphs.empty())
    {
        for (auto& x : _glyphs)
        {
            delete x;
        }
    }
}

void SpriteBatch::init()
{
    createVertexArrayBuffer();
}

void SpriteBatch::begin()
{
    _renderBatches.clear();
    for (int i = 0; i < _glyphs.size(); i++)
    {
        delete _glyphs[i];
    }
    _glyphs.clear();
}

void SpriteBatch::end()
{
    sortGlyphs();
    createRenderBatches();
}

void SpriteBatch::draw(const glm::vec4& destRect, const glm::vec4& srcRect, float depth, GLuint texture, const Color& color)
{
    Glyph* glyph = new Glyph;
    glyph->depth = depth;
    glyph->texture = texture;

    glyph->topLeft.color = color;
    glyph->topLeft.setPosition(destRect.x, destRect.y + destRect.w);
    glyph->topLeft.setUV(srcRect.x, srcRect.y + srcRect.w);

    glyph->bottomLeft.color = color;
    glyph->bottomLeft.setPosition(destRect.x, destRect.y);
    glyph->bottomLeft.setUV(srcRect.x, srcRect.y);

    glyph->bottomRight.color = color;
    glyph->bottomRight.setPosition(destRect.x + destRect.z, destRect.y);
    glyph->bottomRight.setUV(srcRect.x + srcRect.z, srcRect.y);

    glyph->topRight.color = color;
    glyph->topRight.setPosition(destRect.x + destRect.z, destRect.y + destRect.w);
    glyph->topRight.setUV(srcRect.x + srcRect.z, srcRect.y + srcRect.w);


    _glyphs.push_back(glyph);
}

void SpriteBatch::renderBatch()
{
    glBindVertexArray(_vao);
    for (const auto& batch : _renderBatches)
    {
        glBindTexture(GL_TEXTURE_2D, batch.texture);
        glDrawArrays(GL_TRIANGLES, batch.offset, batch.numVerticies);
    }
    glBindVertexArray(0);
}

void SpriteBatch::createVertexArrayBuffer()
{
    if (_vao != 0 || _vbo != 0)
        return;

    glGenVertexArrays(1, &_vao);
    glBindVertexArray(_vao);

    glGenBuffers(1, &_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));
    glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), (void*)offsetof(Vertex, color));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, uv));

    glBindVertexArray(0);
}

void SpriteBatch::sortGlyphs()
{
    switch (_sortType)
    {
        case GlyphSortType::BACK_TO_FRONT:
            std::stable_sort(_glyphs.begin(), _glyphs.end(), [](const Glyph* a, const Glyph* b) -> bool {
                return a->depth > b->depth;
            });
        break;

        case GlyphSortType::FRONT_TO_BACK:
            std::stable_sort(_glyphs.begin(), _glyphs.end(), [](const Glyph* a, const Glyph* b) -> bool {
                return a->depth < b->depth;
            });
        break;

        case GlyphSortType::TEXTURE:
            std::stable_sort(_glyphs.begin(), _glyphs.end(), [](const Glyph* a, const Glyph* b) -> bool {
                return a->texture < b->texture;
            });
        break;

        case GlyphSortType::NONE:
        default:
        break;
    }
}

void SpriteBatch::createRenderBatches()
{
    if (_glyphs.empty())
        return;

    std::vector<Vertex> verticies(_glyphs.size() * 6);

    _renderBatches.push_back({0, 6, _glyphs[0]->texture});

    int offset = 0;
    int currentVertex = 0;

    verticies[currentVertex++] = _glyphs[0]->topLeft;
    verticies[currentVertex++] = _glyphs[0]->bottomLeft;
    verticies[currentVertex++] = _glyphs[0]->bottomRight;
    verticies[currentVertex++] = _glyphs[0]->bottomRight;
    verticies[currentVertex++] = _glyphs[0]->topRight;
    verticies[currentVertex++] = _glyphs[0]->topLeft;
    offset += 6;

    for (int i = 1; i < _glyphs.size(); i++)
    {
        if (_glyphs[i]->texture != _glyphs[i - 1]->texture)
        {
            _renderBatches.emplace_back(offset, 6, _glyphs[i]->texture);
        }
        else
        {
            _renderBatches.back().numVerticies += 6;
        }
        verticies[currentVertex++] = _glyphs[i]->topLeft;
        verticies[currentVertex++] = _glyphs[i]->bottomLeft;
        verticies[currentVertex++] = _glyphs[i]->bottomRight;
        verticies[currentVertex++] = _glyphs[i]->bottomRight;
        verticies[currentVertex++] = _glyphs[i]->topRight;
        verticies[currentVertex++] = _glyphs[i]->topLeft;
        offset += 6;
    }
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, verticies.size() * sizeof(Vertex), nullptr, GL_DYNAMIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, verticies.size() * sizeof(Vertex), verticies.data());
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}